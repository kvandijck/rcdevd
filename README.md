# rcdevd

This is a uevent distribution daemon to /etc/rc.<SUBSYSTEM> scripts.

## Why?

I found that I want some shell script to run
for a limited set of subsystems only.

* Installing something as `/proc/sys/kernel/hotplug` spawns a new process
  for *every* new uevent, which is a serious load during boot.
* Having `udev` is a major footprint increase for my embedded systems.
* Controlling services from withing `udev` rules is very limited.
* It could co-exist with `udev` on a desktop
* Node creation is better done by `devtmpfs`. No need to spend time there
* I don't want to spend cpu or processes for things I don't want to change

## mdev

* `mdev` from `busybox does not support network interfaces
* `mdev` spaws a process for every uevent
