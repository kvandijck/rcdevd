/* See LICENSE file for copyright and license details. */
#include <dirent.h>
#include <errno.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <fcntl.h>
#include <poll.h>
#include <syslog.h>
#include <sys/socket.h>
#include <sys/signalfd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <sys/wait.h>

#include <linux/types.h>
#include <linux/netlink.h>

#include "modalias.h"

#define NAME "rcdevd"

#ifndef UEVENT_BUFFER_SIZE
#define UEVENT_BUFFER_SIZE 16384
#endif

static int max_loglevel = LOG_WARNING;

__attribute((format(printf,2,3)))
void mylog(int loglevel, const char *fmt, ...)
{
	static int logtostderr = -1;
	va_list va;
	char *str;

	if (logtostderr < 0) {
		int fd;

		fd = open("/dev/tty", O_RDONLY | O_NOCTTY);
		if (fd >= 0)
			close(fd);
		/* log to stderr when we're in a terminal */
		logtostderr = fd >= 0;
		if (!logtostderr) {
			openlog(NAME, 0, LOG_LOCAL2);
			setlogmask(LOG_UPTO(max_loglevel));
		}
	}

	/* render string */
	va_start(va, fmt);
	vasprintf(&str, fmt, va);
	va_end(va);

	if (!logtostderr) {
		syslog(loglevel, "%s", str);

	} else if (loglevel <= max_loglevel) {
		struct timespec tv;
		static char timbuf[64];

		clock_gettime(CLOCK_REALTIME, &tv);
		strftime(timbuf, sizeof(timbuf), "%b %d %H:%M:%S", localtime(&tv.tv_sec));
		sprintf(timbuf+strlen(timbuf), ".%03u ", (int)(tv.tv_nsec/1000000));

		struct iovec vec[] = {
			{ .iov_base = timbuf, .iov_len = strlen(timbuf), },
			{ .iov_base = NAME, .iov_len = strlen(NAME), },
			{ .iov_base = ": ", .iov_len = 2, },
			{ .iov_base = str, .iov_len = strlen(str), },
			{ .iov_base = "\n", .iov_len = 1, },
		};
		writev(STDERR_FILENO, vec, sizeof(vec)/sizeof(vec[0]));
	}

	free(str);
	if ((loglevel & LOG_PRIMASK) <= LOG_ERR)
		exit(1);
}
#define ESTR(x) strerror(x)

/* program options */
static const char help_msg[] =
	NAME ": dispatch uevents to /etc/rc.SUBSYSTEM scripts\n"
	"usage:	" NAME " [OPTIONS ...]\n"
	"usage:	" NAME " -A [OPTIONS ...] MODALIAS ...\n"
	"Options\n"
	" -A	Test MODALIAS arguments for module names\n"
	" -c	only coldplug\n"
	" -C	no coldplug\n"
	" -n	dry-run, only print things to do\n"
	" -L	bypass module alias cache, modprobe MODALIAS directly\n"
	" -M	don't modprobe MODALIAS entries\n"
	" -SPREFIX	script prefix, default /etc/rc.\n"
	"		On uevents, the script /etc/rc.SUBSYSTEM will be executed\n"
	" -v	verbose\n"
	;
static const char optstring[] = "+?VvAcCLMn";

/* config */
static int coldplug = 1; /* bit 1 for 'exit after coldplug' */
static int dryrun;
static int modprobe = 1;
static int modaliases = 1;
static const char *prefix = "/etc/rc.";

/* state */
static int coldplug_pending;
static int sigterm;
static sigset_t saved_sigmask;

/* parsed uevent */
static const char **uev;
static int nuev, suev; /* n items, size */

static const char *uevent_find(const char *key)
{
	int j;

	for (j = 0; j < nuev; j += 2) {
		if (!strcmp(key, uev[j]))
			return uev[j+1] ?: "";
	}
	return NULL;
}

static void uevent_add(const char *key, const char *value)
{
	if ((nuev + 2) > suev) {
		suev += 16;
		uev = realloc(uev, sizeof(*uev)*suev);
		if (!uev)
			mylog(LOG_ERR, "realloc %u items: %s", suev, ESTR(errno));
	}
	uev[nuev++] = key;
	uev[nuev++] = value;
}

static void uevent_reset(void)
{
	nuev = 0;
}

static char buf[UEVENT_BUFFER_SIZE *2 +1];
static void handle_uevent(void);
static void uevent_recv(int sk)
{
	int pos, ret;
	char *str, *key, *val;
	struct sockaddr_nl sanl;
	socklen_t sanllen;

	sanllen = sizeof(sanl);
	ret = recvfrom(sk, buf, sizeof(buf) -1, 0, (void *)&sanl, &sanllen);
	if (ret < 0) {
		if (errno == EAGAIN || errno == EINTR)
			return;
		mylog(LOG_ERR, "recv uevent: %s", ESTR(errno));
	}
	if (!ret)
		mylog(LOG_ERR, "uevent EOF?");
	if (sanl.nl_pid != 0)
		/* not kernel event */
		return;

	buf[ret] = 0;

	uevent_reset();
	for (pos = 0; pos < ret; ) {
		str = &buf[pos];
		pos += strlen(str) +1;

		key = str;
		val = strchr(str, '=');
		if (val) {
			*val = 0;
			++val;
		}
		uevent_add(key, val);
	}
	handle_uevent();
}

static int uevent_open_socket(void)
{
	struct sockaddr_nl snl = {
		.nl_family = AF_NETLINK,
		.nl_pid = getpid(),
		.nl_groups = -1,
	};
	int sk;

	sk = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_KOBJECT_UEVENT);
	if (sk < 0)
		mylog(LOG_ERR, "socket NETLINK DGRAM KOBJECT_UEVENT: %s", ESTR(errno));

	if (bind(sk, (struct sockaddr *)&snl, sizeof(snl)) < 0)
		mylog(LOG_ERR, "bind: %s", ESTR(errno));

	if (fcntl(sk, F_SETFD, fcntl(sk, F_GETFD) | FD_CLOEXEC) < 0)
		mylog(LOG_ERR, "setfd CLOEXEC: %s", ESTR(errno));
	return sk;
}

/* modprobe */
static char **modq;
static int nmodq, smodq;

static void run_modprobe_1(const char *modalias)
{
	int pid;

	if (coldplug_pending) {
		/* keep coldplug short, queue modules after coldplug */
		/* lookup first */
		int j;

		for (j = 0; j < nmodq; ++j) {
			if (!strcmp(modq[j], modalias))
				return;
		}
		/* append in queue */
		if (nmodq + 1 > smodq) {
			smodq += 16;
			modq = realloc(modq, sizeof(*modq)*smodq);
			if (!modq)
				mylog(LOG_ERR, "realloc %u modules: %s", smodq, ESTR(errno));
		}
		modq[nmodq++] = strdup(modalias);
		return;
	}

	mylog(LOG_INFO, "$ modprobe -qb %s", modalias);
	if (dryrun)
		return;

	pid = fork();
	if (pid < 0)
		mylog(LOG_WARNING, "fork failed: %s", ESTR(errno));
	else if (pid > 0)
		/* parent */
		return;
	/* child */
	if (sigprocmask(SIG_SETMASK, &saved_sigmask, NULL) < 0)
		mylog(LOG_WARNING, "child: sigprocmask: %s", ESTR(errno));
	execlp("modprobe", "modprobe", "-qb", modalias, NULL);
	mylog(LOG_ERR, "execlp modprobe %s: %s", modalias, ESTR(errno));
}

static void run_modprobe(const char *modalias)
{
	static int aliases_loaded = 0;
	const char *module;

	if (!modaliases) {
		/* bypass modalias lookup */
		run_modprobe_1(modalias);
		return;
	}

	if (!aliases_loaded) {
		mylog(LOG_NOTICE, "loading modalias");
		if (modalias_load(NULL) >= 0)
			aliases_loaded = 1;
	}

	while ((module = modalias_to_module(modalias)) != NULL) {
		if (module != modalias)
			run_modprobe_1(module);
	}
}

static void flush_modq(void)
{
	int j;

	for (j = 0; j < nmodq; ++j) {
		run_modprobe_1(modq[j]);
		free(modq[j]);
	}
	free(modq);
	modq = NULL;
	nmodq = smodq = 0;
}

/* handler */
static void handle_uevent(void)
{
	int ret, j;
	const char *ss, *dev, *action, *mod;
	const char *drv;
	static char script[1024];
	struct stat st;

	dev = uevent_find("DEVNAME");
	if (!dev) {
		dev = uevent_find("DEVPATH");
		if (dev)
			dev = strrchr(dev, '/')+1;
		if (dev == (void *)1L)
			dev = NULL;
		if (dev)
			uevent_add("DEVNAME", dev);
	}
	action = uevent_find("ACTION");
	ss = uevent_find("SUBSYSTEM");
	drv = uevent_find("DRIVER");

	mylog(LOG_DEBUG, "%s (%s) %s", action ?: "?", ss ?: "?", dev ?: "?");

	if (modprobe && !drv && !strcmp(action, "add")) {
		mod = uevent_find("MODALIAS");
		if (mod)
			run_modprobe(mod);
	}

	if (!ss)
		return;

	sprintf(script, "%s%s", prefix, ss);
	if (stat(script, &st) < 0)
		return;

	mylog(LOG_INFO, "$ %s %s %s", script, dev, action);
	if (dryrun)
		return;

	ret = fork();
	if (ret < 0)
		mylog(LOG_WARNING, "fork failed: %s", ESTR(errno));
	else if (ret > 0)
		/* parent */
		return;
	/* child */
	if (sigprocmask(SIG_SETMASK, &saved_sigmask, NULL) < 0)
		mylog(LOG_WARNING, "child: sigprocmask: %s", ESTR(errno));
	/* prepare environment */
	for (j = 0; j < nuev; j += 2)
		/* add environment, avoid NULL value strings */
		setenv(uev[j], uev[j+1] ?: "", 1);
	execlp(script, script, dev, action, NULL);
	mylog(LOG_ERR, "execlp %s %s %s: %s", script, dev, action, ESTR(errno));
}

/* COLDPLUG */
static int sysfs_read(const char *dir, const char *property, char *buf, int len, int allow_enoent)
{
	static char propfile [1024*4];
	int fd;
	int ret;
	char *retstr;
	struct stat st;

	snprintf(propfile, sizeof(propfile), "%s/%s", dir, property);
	ret = lstat(propfile, &st);
	if (ret < 0) {
		if (!allow_enoent || (errno != ENOENT))
			mylog(LOG_WARNING, "lstat %s: %s", propfile, ESTR(errno));
		return -1;
	}
	if (S_ISLNK(st.st_mode)) {
		ret = readlink(propfile, buf, len-1);
		if (ret < 0) {
			mylog(LOG_WARNING, "readlink %s: %s", propfile, ESTR(errno));
			return -1;
		}
		buf[ret] = 0;
		retstr = strrchr(buf, '/');
		if (retstr)
			memmove(buf, retstr+1, strlen(retstr+1)+1);
		return strlen(buf);
	}

	fd = open(propfile, O_RDONLY);
	if (fd < 0) {
		mylog(LOG_WARNING, "open %s rd: %s", propfile, ESTR(errno));
		return -1;
	}
	ret = read(fd, buf, len-1);
	close(fd);
	if (ret < 0) {
		mylog(LOG_WARNING, "read %s: %s", propfile, ESTR(errno));
		return -1;
	}
	// add null terminator
	buf[ret] = 0;
	/* remove trailing newline */
	for (retstr = &buf[ret -1]; retstr >= buf; --retstr) {
		if (!strchr(" \t\r\n\v\f", *retstr))
			break;
		*retstr = 0;
	}
	return strlen(buf);
}

static int compose_cold_uevent(const char *dir)
{
	char *str = buf;
	char *ebuf = buf+sizeof(buf);
	int ret;
	char *line, *key, *val;

	memset(buf, 0, sizeof(buf));
	uevent_reset();
	uevent_add("ACTION", "add");

	uevent_add("DEVPATH", dir+5 /* omit '/sys/' */);

	ret = sysfs_read(dir, "uevent", str, ebuf-str, 1);
	if (ret < 0)
		/* this does not look like a device sysfs dir */
		return -1;
	/* parse uevent */
	for (line = str; line && *line; ) {
		key = line;
		line = strchr(line, '\n');
		if (line)
			*line++ = 0;
		val = strchr(key, '=');
		if (val)
			*val++ = 0;
		uevent_add(key, val);
	}
	str += ret+1;

	/* find subsystem from symlinks */
	if (sysfs_read(dir, "subsystem", str, ebuf-str, 1) > 0) {
		uevent_add("SUBSYSTEM", str);
		str += strlen(str) + 1;
	}

	if (sysfs_read(dir, "driver", str, ebuf-str, 1) > 0) {
		uevent_add("DRIVER", str);
		str += strlen(str) + 1;
	}

	return 0;
}

static int coldplug_device(const char *dirname)
{
	DIR *dir;
	struct dirent *ent;
	char *subdir;
	int ret;
	int cnt = 0;

	ret = compose_cold_uevent(dirname);
	if (ret >= 0) {
		handle_uevent();
		++cnt;
	}

	/* recurse into deeper directories */
	dir = opendir(dirname);
	if (!dir) {
		mylog(LOG_WARNING, "opendir %s: %s", dirname, ESTR(errno));
		return cnt;
	}
	while (0 != (ent = readdir(dir))) {
		if (ent->d_name[0] && ('.' == ent->d_name[0]))
			continue;
		if (DT_DIR != ent->d_type)
			continue;
		asprintf(&subdir, "%s/%s", dirname, ent->d_name);
		cnt += coldplug_device(subdir);
		free(subdir);
	}
	closedir(dir);
	return cnt;
}

static void do_coldplug(void)
{
	mylog(LOG_INFO, "coldplug ...");
	setenv("COLDPLUG", "1", 1);
	coldplug_pending = 1;
	coldplug_device("/sys/devices");
	coldplug_pending = 0;
	unsetenv("COLDPLUG");
	flush_modq();
}

static void signal_recvd(int fd)
{
	int ret;
	int status;
	struct signalfd_siginfo sfdi;

	for (;;) {
		ret = read(fd, &sfdi, sizeof(sfdi));
		if (ret < 0 && errno == EAGAIN)
			break;
		if (ret < 0)
			mylog(LOG_ERR, "read signalfd: %s", ESTR(errno));
		switch (sfdi.ssi_signo) {
		case SIGTERM:
		case SIGINT:
			sigterm = 1;
			break;
		case SIGCHLD:
			while (waitpid(-1, &status, WNOHANG) > 0);
			break;
		}
	}
}

/* main process */
int main(int argc, char *argv[])
{
	int opt;
	int sk;
	int modaliastest = 0;

	/* parse program options */
	while ((opt = getopt(argc, argv, optstring)) != -1)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s: %s\n", NAME, VERSION);
		return 0;
	default:
		fprintf(stderr, "%s: option '%c' unrecognised\n", NAME, opt);
	case '?':
		fputs(help_msg, stderr);
		exit(1);
		break;
	case 'v':
		++max_loglevel;
		break;

	case 'A':
		modaliastest = 1;
		break;
	case 'c':
		coldplug |= 2;
		break;
	case 'C':
		coldplug = 0;
		break;
	case 'L':
		modaliases = 0;
		break;
	case 'M':
		modprobe = 0;
		break;
	case 'n':
		dryrun = 1;
		break;
	case 'S':
		prefix = optarg;
		break;
	}

	if (modaliastest) {
		/* test modalias */
		const char *module, *alias;

		modalias_load(getenv("MODALIAS_FILE"));

		for (; optind < argc; ++optind) {
			alias = argv[optind];

			printf("%s:", alias);
			while ((module = modalias_to_module(alias)) != NULL) {
				if (alias != module)
					printf(" %s", module);
			}
			printf("\n");
		}
		return 0;
	}

	sk = uevent_open_socket();

	sigset_t sigmask;
	int sigfd;

	sigfillset(&sigmask);

	if (sigprocmask(SIG_BLOCK, &sigmask, &saved_sigmask) < 0)
		mylog(LOG_ERR, "sigprocmask: %s", ESTR(errno));
	sigfd = signalfd(-1, &sigmask, SFD_NONBLOCK | SFD_CLOEXEC);
	if (sigfd < 0)
		mylog(LOG_ERR, "signalfd failed: %s", ESTR(errno));

	if (coldplug & 1)
		do_coldplug();
	if (coldplug & 2)
		return 0;

	mylog(LOG_INFO, "waiting for uevents ...");

	struct pollfd pfd[2] = {
		{
			.fd = sigfd,
			.events = POLLIN,
		}, {
			.fd = sk,
			.events = POLLIN,
		},
	};

	for (; !sigterm;) {
		if (poll(pfd, 2, -1) < 0)
			mylog(LOG_ERR, "poll: %s", ESTR(errno));
		if (pfd[0].revents)
			signal_recvd(sigfd);
		if (pfd[1].revents)
			uevent_recv(sk);
	}

	/* keep valgrind happy */
	modalias_unload();
	if (uev)
		free(uev);

	return 0;
}
