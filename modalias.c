#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <sys/stat.h>
#include <sys/utsname.h>
#include <syslog.h>

#include "modalias.h"

static char **table;
static int ntable, stable; /* fill and size */

static inline const char *of_next(const char *haystack)
{
	return strpbrk(haystack+1, "ABCDEFGHIJKLMNOPQRSTUVWXYZ") ?: &haystack[strlen(haystack)];
}

static inline long capflag(int chr)
{
	return 1 << (chr - 'A');
}

int modalias_strcmp_of(const char *ref, const char *dut)
{
	const char *tok, *next;
	const char *dutkey, *nextdutkey;
	long okflags = 0, failflags = 0, flag;

	for (; ref && *ref; ref = next) {
		next = of_next(ref);
		flag = capflag(*ref);
		if (*ref && ref[1] == '*') {
			okflags |= flag;
			/* match any, never fail */
			continue;
		}
		for (dutkey = strchr(dut, *ref); dutkey && *dutkey;
				dutkey = strchr(nextdutkey, *ref)) {
			nextdutkey = of_next(dutkey);
			if (((next - ref) == (nextdutkey - dutkey)) &&
					!strncmp(ref, dutkey, next - ref)) {
				okflags |= flag;
				break;
			} else
				failflags |= flag;
		}
	}
	failflags &= ~okflags;
	return !(okflags && !failflags);
}

int modalias_strcmp(const char *ref, const char *dut)
{
	static const char hex_hi [] = "0123456789ABCDEF";
	char *str;

	/* OpenFirmware matches are little more complex */
	if (!strncmp(dut, "of:", 3) && !strncmp(ref, "of:", 3))
		return modalias_strcmp_of(ref+3, dut+3);

	for (; *dut && *ref; ++ref) {
		if (*ref == '*') {
			if (!strchr(hex_hi, *dut))
				goto failed;
			for (++dut; *dut; ++dut) {
				if (!strchr(hex_hi, *dut))
					break;
			}
		} else if (*ref != *dut) {
			if (strchr("-_", *ref) && strchr("-_", *ref))
				goto dashes_differ;
			goto failed;
		} else {
dashes_differ:
			++dut;
		}
	}
	if (!*dut && ('*' == *ref))
		// modalias wildcard at the end
		++ref;
	if (!*ref && *dut) {
		if (strpbrk(dut, hex_hi))
			// fail when the remainder contains some identifactions yet
			goto failed;
		return 0;
	}
	if (*dut || *ref)
		goto failed;
	return 0;
failed:
	return -1;
}

static int strhash(const char *str)
{
	int hash = 0;

	for (; *str; ++str)
		hash = (hash ^ (*str + (*str << 8))) << 1;
	return hash;
}

/* return module name from MODALIAS
 * run multiple times with same alias,
 * some alias yield multiple matches
 */
const char *modalias_to_module(const char *alias)
{
	static int savedpos;
	int pos;
	static int saved_alias_hash;
	int alias_hash;

	if (!alias || !*alias)
		return NULL;

	/* compare hash, and restart lookup if new alias is given */
	alias_hash = strhash(alias);
	if (alias_hash != saved_alias_hash) {
		/* reset search position */
		savedpos = 0;
		saved_alias_hash = alias_hash;
	} else if (!savedpos) {
		/* previous run didn't return anything, so terminate here */
		return NULL;
	}

	/* find alias */
	for (pos = savedpos; pos < ntable; pos += 2) {
		if (!modalias_strcmp(table[pos], alias)) {
			savedpos = pos + 2;
			return table[pos+1];
		}
	}
	/* nothing found */
	if (savedpos)
		/* terminate list */
		return NULL;
	else
		/* return alias himself */
		return alias;
}

void modalias_unload(void)
{
	int j;

	for (j = 0; j < ntable; ++j)
		free(table[j]);
	free(table);
	table = NULL;
	ntable = stable = 0;
}

int modalias_load(const char *altfile)
{
	int ret;
	char *file;
	struct stat st;
	struct utsname uts;

	if (altfile)
		file = (char *)altfile;
	else {
		uname(&uts);
		asprintf(&file, "/lib/modules/%s/modules.alias", uts.release);
	}
	if (stat(file, &st) < 0) {
		mylog(LOG_WARNING, "file '%s': %s", file, strerror(errno));
		if (altfile)
			return -1;
		goto done;
	}

	FILE *fp;
	char *line = NULL;
	size_t linesize = 0;

	fp = fopen(file, "r");
	if (!fp) {
		mylog(LOG_WARNING, "fopen '%s': %s", file, strerror(errno));
		goto done;
	}

	char *tok, *value;
	for (;;) {
		ret = getline(&line, &linesize, fp);
		if (ret <= 0) {
			if (!feof(fp))
				mylog(LOG_WARNING, "fread '%s': %s", file, strerror(errno));
			break;
		}
		if (*line == '#')
			continue;
		tok = strtok(line, " \t\v\f\r\n");
		if (!tok || strcmp(tok, "alias"))
			continue;
		tok = strtok(NULL, " \t\v\f\r\n");
		value = strtok(NULL, " \t\v\f\r\n");

		if (!value)
			continue;
		if (strlen(tok) >= 2 && !strcmp("C*", tok+strlen(tok)-2))
			/* ignore the duplicate line with trailing C* */
			continue;
		/* ok, we got a new alias */
		if ((ntable + 2) > stable) {
			stable += 128;
			table = realloc(table, stable*sizeof(*table));
		}
		table[ntable++] = strdup(tok);
		table[ntable++] = strdup(value);
	}

	if (line)
		free(line);
	fclose(fp);

done:
	if (file && file != altfile)
		free(file);
	return 0;
}
