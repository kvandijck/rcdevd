#ifndef modalias_h
#define modalias_h

#ifdef __cplusplus
extern "C" {
#endif

/* load database (NULL for default) */
extern int modalias_load(const char *file);
/* unload database */
extern void modalias_unload(void);
/* get module name */
extern const char *modalias_to_module(const char *dut);
/* core compare function */
extern int modalias_strcmp(const char *ref, const char *dut);

/* imports */
extern
__attribute((format(printf,2,3)))
void mylog(int loglevel, const char *fmt, ...);

#ifdef __cplusplus
}
#endif

#endif
